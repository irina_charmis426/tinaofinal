﻿$(document).ready(function() {
	var api;
	var throttleTimeout;
	//Скроллы
	$(function()
	{
		$('.address_table_wp').each(
			function()
			{
				$(this).jScrollPane(
					{
						showArrows: false
					}
				);
				api = $(this).data('jsp');


				$("#hide_link").click(function () {
                    setTimeout(api_reinit, 300);
				});

				$(window).bind(
					'resize',
					function()
					{
						if (!throttleTimeout) {
							throttleTimeout = setTimeout(
								function()
								{
									api.reinitialise();
									throttleTimeout = null;
								},
								10
							);
						}
					}
				);
			}
		)

	});

	$(function()
	{
		$('.grafik_table_wp').each(
			function()
			{
				$(this).jScrollPane(
					{
						showArrows: false
					}
				);
				api = $(this).data('jsp');

				$("#hide_link").click(function () {
					setTimeout(api_reinit, 300);
				});

				$(window).bind(
					'resize',
					function()
					{
						if (!throttleTimeout) {
							throttleTimeout = setTimeout(
								function()
								{
									api.reinitialise();
									throttleTimeout = null;
								},
								10
							);
						}
					}
				);
			}
		)

	});

	$(function()
	{
		$('.application_table_wp').each(
			function()
			{
				$(this).jScrollPane(
					{
						showArrows: false
					}
				);
				api = $(this).data('jsp');


				$("#hide_link").click(function () {
					setTimeout(api_reinit, 300);
				});
				$(window).bind(
					'resize',
					function()
					{
						if (!throttleTimeout) {
							throttleTimeout = setTimeout(
								function()
								{
									api.reinitialise();
									throttleTimeout = null;
								},
								10
							);
						}
					}
				);
			}
		)

	});

	$(function()
	{
		$('.application_table_wp2').each(
			function()
			{
				$(this).jScrollPane(
					{
						showArrows: false
					}
				);
				api = $(this).data('jsp');


				$("#hide_link").click(function () {
					setTimeout(api_reinit, 300);
				});

				$(window).bind(
					'resize',
					function()
					{
						if (!throttleTimeout) {
							throttleTimeout = setTimeout(
								function()
								{
									api.reinitialise();
									throttleTimeout = null;
								},
								10
							);
						}
					}
				);
			}
		)

	});



	$('#sort_table').DataTable( {
		paging: false,
		searching: false,
		info: false,
		bSortCellsTop: true
	} );
	//приклеивание шапки таблицы при скролле
	var trp_del;
	head_res = true;
	$(window).scroll(function(){
		scroller_del = $('#scroller');
		scroller_del0 = scroller_del[0];
		trn_del = scroller_del0.getBoundingClientRect();
		trp_del = trn_del.top;
		if(trp_del <= 0){
			$('thead').css('top', -trp_del-10);
			$('thead').css('padding-top', '40px');
			$('.address_table').css('padding-top', '110px');
			$('[class^="jspHorizontalBar"]').each(function(){
				$(this).css('position', 'fixed');
				this_parent = $(this).closest('.jspContainer');
				$(this).width(this_parent.width());
				$(this).css('left', this_parent.offset().left);
				$(this).css('top', '10px');
			});
		}
		else if(trp_del > 0){
			$('thead').css('top', '40px');
			$('thead').css('padding-top', '0');
			$('.address_table').css('padding-top', '70px');
			$('[class^="jspHorizontalBar"]').each(function(){
				$(this).css('position', 'absolute');
				this_parent = $(this).closest('.jspContainer');
				$(this).width(this_parent.width());
				$(this).css('left', '0');
				$(this).css('top', '0');
				$(this).css('top', '0');
			});
		}
	});




	//Модальные окна
	$(".link_1").click(function () {
		$('#small-modal1').arcticmodal();
	});
	$(".link_2").click(function () {
		$('#small-modal2').arcticmodal();
	});
	$('.expand_link').click(function(){
		if ($(this).hasClass("expand_inactive")) {
			expand_height = $(this).closest("li").find("ul").height();
			$(this).closest("li").find(".expand_outer").animate({height: expand_height}, 300);
			$(this).closest("li").find(".fa-angle-right").addClass("fa-angle-down");
			$(this).closest("li").find(".fa-angle-down").removeClass("fa-angle-right");
			$(this).addClass("expand_active");
			$(this).removeClass("expand_inactive");
			$(this).closest("li").addClass("lm_bg1");
		}
		else if ($(this).hasClass("expand_active")) {
			this_item = $(this);
			$(this).closest("li").find(".expand_outer").animate({height: 0}, 300);
			$(this).closest("li").find(".fa-angle-down").addClass("fa-angle-right");
			$(this).closest("li").find(".fa-angle-right").removeClass("fa-angle-down");
			$(this).addClass("expand_inactive");
			$(this).removeClass("expand_active");
			function bg_remove(){
				this_item.closest("li").removeClass("lm_bg1");
			}
			setTimeout(bg_remove, 300);
		}
	});

	function api_reinit() {
		api.reinitialise();
		throttleTimeout = null;
	}
    function hidden() {
		$(".toggle_hidden").css("display", "none");
	}
	function hidden2() {
		$(".container-fluid1").animate({paddingLeft: 60}, 300);
		$(".container-fluid2").animate({paddingLeft: 60}, 300);
		$(".container-fluid3").animate({paddingLeft: 60}, 300);
	}
	$('.toggle_menu a').click(function(){
		if ($(this).hasClass("expand_inactive")) {
			$(".left_menu").animate({width: 32}, 300);
			$(".left_menu").addClass('active');
			setTimeout(hidden, 300);
			setTimeout(hidden2, 1);
			$(this).addClass("expand_active");
			$(this).removeClass("expand_inactive");
			//$('.expand_outer_2').height(0);
			//$('.toggle_main').addClass('toggle_main_inactive');
			//$('.toggle_main').removeClass('toggle_main_active');
			$('.expand_outer_2').css('height', 'auto');
			$('.expand_outer').height(0);
			$('.expand_link').addClass('expand_inactive');
			$('.expand_link').removeClass('expand_active');
			$('.toggle_hidden').removeClass('lm_bg1');
			$('.expand_link .fa').removeClass('fa-angle-down');
			$('.expand_link .fa').addClass('fa-angle-right');
		}
		else if ($(this).hasClass("expand_active")) {
			$(".left_menu").animate({width: 250}, 300);
			$(".left_menu").removeClass('active');
			$(".container-fluid1").animate({paddingLeft: 280}, 300);
			$(".container-fluid2").animate({paddingLeft: 250}, 300);
			$(".container-fluid3").animate({paddingLeft: 280}, 300);
			$(".toggle_hidden").css("display", "block");
			$(this).addClass("expand_inactive");
			$(this).removeClass("expand_active");
			//$('.expand_outer_2').height(0);
			//$('.toggle_main').addClass('toggle_main_inactive');
			//$('.toggle_main').removeClass('toggle_main_active');
			$('.expand_outer_2').css('height', 'auto');
			$('.expand_outer').height(0);
			$('.expand_link').addClass('expand_inactive');
			$('.expand_link').removeClass('expand_active');
			$('.toggle_hidden').removeClass('lm_bg1');
			$('.expand_link .fa').removeClass('fa-angle-down');
			$('.expand_link .fa').addClass('fa-angle-right');
		}
	});

	$('.st_choice_block>input').change(function(){
		if ($(this).prop('checked')) {
			$('.st_choice_block').find('div').find('input').prop('checked', true)
		}
		else {
			$('.st_choice_block').find('div').find('input').prop('checked', false)
		}
	});
	$('#table_check').change(function(){
		if ($(this).prop('checked')) {
			$('.app2_td1').find('input').prop('checked', true)
		}
		else {
			$('.app2_td1').find('input').prop('checked', false)
		}
	});


	$('.toggle_main').click(function(){
		if ($(this).hasClass("toggle_main_inactive")) {
			expand_height = $(this).closest("li").find("ul").height();
			$(this).closest("li").find(".expand_outer_2").animate({height: expand_height}, 300);
			$(this).addClass("toggle_main_active");
			$(this).removeClass("toggle_main_inactive");
		}
		else if ($(this).hasClass("toggle_main_active")) {

			$(this).closest("li").find(".expand_outer_2").animate({height: 0}, 300);
			$(this).addClass("toggle_main_inactive");
			$(this).removeClass("toggle_main_active");

		}
	});

	//табы в графике
	$('.tab_link').click(function(){
		if ($(this).hasClass("inactive")) {
			target = $(this).attr('data-target');
			$('.tabs').css('display', 'none');
			$('.tabs').css('opacity', '0');
			$('#'+target).css('display', 'block');
			$('#'+target).animate({opacity: 1}, 300);
			$('.tab_link').addClass("inactive");
			$('.tab_link').removeClass("active");
			$(this).addClass("active");
			$(this).removeClass("inactive");
			$('.application_table_wp2').width('100%');
			$('.jspContainer').width('100%');
		}
	});

	$('.select_text').click(function(e){
		if ($(this).hasClass("inactive")) {
			e.stopPropagation();
			this_item = $(this).closest(".select_block").find('.st_choice_block');
			$('.select_text').closest(".select_block").find('.select_toggle_block').css('display', 'none');
			$('.select_text').closest(".select_block").find('.select_toggle_block').animate({opacity: 0}, 400);
			$('.select_text').addClass("inactive");
			$('.select_text').removeClass("active");
			$(this).closest(".select_block").find('.select_toggle_block').css('display', 'block');
			$(this).closest(".select_block").find('.select_toggle_block').animate({opacity: 1}, 400);
			$(this).addClass("active");
			$(this).removeClass("inactive");
			$(function()
			{
				$(this_item).each(
					function()
					{
						$(this).jScrollPane(
							{
								showArrows: false
							}
						);
						var api = $(this).data('jsp');
						var throttleTimeout;
						/*
						 $("#hide_link").click(function () {
						 api.reinitialise();
						 throttleTimeout = null;
						 });
						 */
						$(window).bind(
							'resize',
							function()
							{
								if (!throttleTimeout) {
									throttleTimeout = setTimeout(
										function()
										{
											api.reinitialise();
											throttleTimeout = null;
										},
										10
									);
								}
							}
						);
					}
				)

			});
		}
		else if ($(this).hasClass("active")) {
			e.stopPropagation();
			$('.select_text').closest(".select_block").find('.select_toggle_block').css('display', 'none');
			$('.select_text').closest(".select_block").find('.select_toggle_block').animate({opacity: 0}, 400);
			$(this).addClass("inactive");
			$(this).removeClass("active");
		}
	});

	/*
	$('body').click(function(e){
		e.stopPropagation();
		$('.select_text').closest(".select_block").find('.select_toggle_block').css('display', 'none');
		$('.select_text').closest(".select_block").find('.select_toggle_block').animate({opacity: 0}, 400);
		$('.select_text').addClass("inactive");
		$('.select_text').removeClass("active");
	});
	*/


	$(function() {
		$( "#datepicker" ).datepicker( $.datepicker.regional[ "ru" ] );
	});
	$(function() {
		$( "#datepicker_2" ).datepicker( $.datepicker.regional[ "ru" ] );
	});
	( function( factory ) {
		if ( typeof define === "function" && define.amd ) {

			// AMD. Register as an anonymous module.
			define( [ "../widgets/datepicker" ], factory );
		} else {

			// Browser globals
			factory( jQuery.datepicker );
		}
	}( function( datepicker ) {

		datepicker.regional.ru = {
			closeText: "Закрыть",
			prevText: "&#x3C;Пред",
			nextText: "След&#x3E;",
			currentText: "Сегодня",
			monthNames: [ "Январь","Февраль","Март","Апрель","Май","Июнь",
				"Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь" ],
			monthNamesShort: [ "Янв","Фев","Мар","Апр","Май","Июн",
				"Июл","Авг","Сен","Окт","Ноя","Дек" ],
			dayNames: [ "воскресенье","понедельник","вторник","среда","четверг","пятница","суббота" ],
			dayNamesShort: [ "вск","пнд","втр","срд","чтв","птн","сбт" ],
			dayNamesMin: [ "Вс","Пн","Вт","Ср","Чт","Пт","Сб" ],
			weekHeader: "Нед",
			dateFormat: "dd.mm.yy",
			firstDay: 1,
			isRTL: false,
			showMonthAfterYear: false,
			yearSuffix: "" };
		datepicker.setDefaults( datepicker.regional.ru );

		return datepicker.regional.ru;

	} ) );


	$('.select').styler();
	$('.select2').styler();
	$('.select3').styler();
	$('.browse').styler();
});

$('.g-calendar').datepicker({
	language: "ru",
	todayHighlight: true
});